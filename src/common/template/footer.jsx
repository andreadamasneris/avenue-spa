import React from 'react'

export default props => (
    <footer className='main-footer'>
        <strong>
            Copyright &copy; 2018
            <a href='http://avenuesecurities.com/' target='_blank'> Avenue Securities</a>.
        </strong>
    </footer>
)