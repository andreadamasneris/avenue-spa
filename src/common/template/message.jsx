import React from 'react';

export default props => (
  <div>
    <h3>{props.message}</h3>
  </div>
)