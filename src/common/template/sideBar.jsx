import React, { Component } from "react";

import Menu from './menu'
import Routes from "../../main/index";

class Sidebar extends Component {
    render() {
        return (
            <aside className='main-sidebar'>
                <section className='sidebar'>
                    <Menu />
                </section>
            </aside>
        )
    }
}

export default Sidebar;
