import React from 'react'

export default props => (
    <div className="form-group">
        <label>{props.label}</label>
        <div className="input-group date">
            <div className="input-group-addon">
                <i className="fa fa-calendar"></i>
            </div>
            <input type="text" className="form-control pull-right" id="datepicker"
                placeholder="mm/dd/yyyy"
                name={props.name}
                onChange={props.onChange}
                value={props.value} />
        </div>
    </div>
)