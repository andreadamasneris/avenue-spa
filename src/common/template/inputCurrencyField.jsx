import React from 'react'

export default props => (
    <div className="form-group">
        <label>{props.label}</label>
        <div className="input-group money">
            <span className="input-group-addon">$</span>
            <input type="text" className="form-control"
                placeholder="1.000"
                name={props.name}
                onChange={props.onChange}
                value={props.value} />
            <span className="input-group-addon">.00</span>
        </div>
    </div>
)