import React from 'react'

export default props => (
    <header className='main-header'>
        <a href='/#/' className='logo'>
            <span className='logo-mini'><b>{props.firstName.charAt(0)}</b>{props.secondName.charAt(0)}</span>
            <span className='logo-lg'>
                <i className={props.icon}></i>
                <b> {props.firstName}</b> {props.secondName}
            </span>
        </a>
        <nav className='navbar navbar-static-top'>
            <a href className='sidebar-toggle' data-toggle='offcanvas'></a>
        </nav>
    </header>
)