import React, { Component } from "react";
import { NavLink } from "react-router-dom";

import Routes from "../../main/index";

export default props => (
    <div>
        <ul className='sidebar-menu'>
            {Routes.map((prop, key) => {
                return (
                    <li key={key}>
                        <NavLink to={prop.path}>
                            <i className={prop.icon} />
                            <span><strong>{prop.name}</strong></span>
                        </NavLink>
                    </li>
                );
            })}
        </ul>
    </div>
)