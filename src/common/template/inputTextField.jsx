import React from 'react'

export default props => (
    <div className="form-group">
        <label>{props.label}</label>
        <input type="text" className="form-control"
            placeholder={props.placeholder}
            name={props.name}
            onChange={props.onChange}
            value={props.value} />
    </div>
)
