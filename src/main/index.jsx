import Watch from "../watch/watch";
import Registration from "../registration/registration";

const Routes = [
  {
    path: "/watch",
    name: "Watch",
    icon: "fa fa-line-chart",
    component: Watch
  },
  {
    path: "/registration",
    name: "Registration",
    icon: "fa fa-user",
    component: Registration
  }
];

export default Routes;
