import React, { Component } from "react";
import { Route, Switch, Redirect } from "react-router-dom";

import '../common/js/dependencies'
import Header from '../common/template/header'
import SideBar from '../common/template/sideBar'
import Footer from '../common/template/footer'
import Routes from "./index";

export default props => (
    <div className="wrapper">
        <Header icon="fa fa-dollar" firstName="Avenue" secondName="Securities" />
        <SideBar />
        <div className='content-wrapper'>
            <Switch>
                {Routes.map((prop, key) => {
                    return (
                        <Route path={prop.path} component={prop.component} key={key} />
                    );
                })}
            </Switch>
        </div>
        <Footer />
    </div>
)