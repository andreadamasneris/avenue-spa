import React from 'react'

export default props => {

    const renderRows = () => {
        const list = props.list || []
        const symbol = props.symbol

        return list.map((item, index) => (
            <tr key={index}>
                <td>{item.date}</td>
                <td>{item.open}</td>
                <td>{item.high}</td>
                <td>{item.low}</td>
                <td>{item.close}</td>
                <td>{item.volume}</td>
            </tr>
        ))
    }

    return (
        <div className="box">
            <div className="box-header">
                <h3 className="box-title">Weekly</h3>
            </div>
            <div className="box-body table-responsive no-padding">
                <table className="table table-hover">
                    <thead>
                        <tr>
                            <th>Date</th>
                            <th>Open</th>
                            <th>High</th>
                            <th>Low</th>    
                            <th>Close</th>    
                            <th>Volume</th>
                        </tr>
                    </thead>
                    <tbody>
                        {renderRows()}
                    </tbody>
                </table>
            </div>
        </div>
    )
}


