import React, { Component } from 'react'
import { Bar } from 'react-chartjs-2'

export default props => {

    const list = props.list || []
    const symbol = props.symbol + ' - Volume'

    const data = {
        labels: list.map((item) => item.date),
        datasets: [
          {
            label: symbol,
            backgroundColor: 'rgba(255,99,132,0.2)',
            borderColor: 'rgba(255,99,132,1)',
            borderWidth: 1,
            hoverBackgroundColor: 'rgba(255,99,132,0.4)',
            hoverBorderColor: 'rgba(255,99,132,1)',
            data: list.map((item) => item.volume)
        }
        ]
      }

    return (
        <Bar data={data} />
    )
}