import React, { Component } from 'react'
import Axios from 'axios'

import Message from '../common/template/message';
import Loading from '../common/template/loading';
import ContentHeader from '../common/template/contentHeader'
import Content from '../common/template/content'

import WatchForm from './watchForm'
import WatchList from './watchList'
import WatchGraphLine from './watchGraphLine'
import WatchGraphBar from './watchGraphBar'

//Api Alpha Vantage
const apiKey = '1L0Y4ZD7JPR6MWLZ'
const baseURL = 'https://www.alphavantage.co/'
const functionURL = 'TIME_SERIES_DAILY'
const dictionaryKey = 'Time Series (Daily)'
const symbolEntry = 'MSFT'

class Watch extends Component {

    constructor(props) {
        super(props)
        this.state = { symbol: symbolEntry, isLoading: true, list: [] }

        this.handleOnChange = this.handleOnChange.bind(this)

        this.getData(this.state.symbol)
    }

    handleOnChange(e) {
        this.setState({ ...this.state, symbol: e.target.value, isLoading: true })
        this.getData(e.target.value)
    }

    getData(psymbol = '') {
        const url = `${baseURL}query?function=${functionURL}&symbol=${psymbol}&apikey=${apiKey}`
        Axios.get(url)
            .then(resp => this.parseData(resp, psymbol))
            .catch(error => {
                console.log(error);
            })
    }

    parseData(resp, psymbol) {
        const weeklyData = resp.data[dictionaryKey]
        var result = [];
        for (var i in weeklyData) {
            var item = { 
                date: i, 
                open: weeklyData[i]['1. open'], 
                high: weeklyData[i]['2. high'], 
                low: weeklyData[i]['3. low'], 
                close: weeklyData[i]['4. close'], 
                volume: weeklyData[i]['5. volume'] 
            }
            result.push(item)
        }
        var finalData = result.slice(0, 5)

        this.setState({ ...this.state, symbol: psymbol, isLoading: false, list: finalData })
    }

    render() {

        if (this.state.isLoading) {
            return (
                <div>
                    <ContentHeader title="Watch" />
                    <Content>
                        <WatchForm
                            symbol={this.state.symbol}
                            handleOnChange={this.handleOnChange}
                        />
                        <Loading />
                    </Content>
                </div>
            )
        }

        if (this.state.list.length <= 0) {
            return (
                <div>
                    <ContentHeader title="Watch" />
                    <Content>
                        <WatchForm
                            symbol={this.state.symbol}
                            handleOnChange={this.handleOnChange}
                        />
                        <Message
                            message='Oops! Unavailable service...'
                        />
                    </Content>
                </div>
            )
        }

        return (
            <div>
                <ContentHeader title="Watch" />
                <Content>
                    <WatchForm
                        symbol={this.state.symbol}
                        handleOnChange={this.handleOnChange}
                    />

                    <div className="space">
                        <div className="row">
                            <div className="col-lg-12">
                                <WatchList
                                    symbol={this.state.symbol}
                                    list={this.state.list}
                                />
                            </div>
                        </div>
                    </div>

                    <div className="space">
                        <div className="row">
                            <div className="col-lg-6">
                                <WatchGraphLine
                                    symbol={this.state.symbol}
                                    list={this.state.list}
                                />
                            </div>
                            <div className="space">
                            </div>
                            <div className="col-lg-6">
                                <WatchGraphBar
                                    symbol={this.state.symbol}
                                    list={this.state.list}
                                />
                            </div>
                        </div>
                    </div>

                    {/* <div className="space">
                        <div className="row">
                            <div className="col-lg-6">
                                <WatchGraphLine
                                    symbol={this.state.symbol}
                                    list={this.state.list}
                                />
                            </div>
                            <div className="space">
                            </div>
                            <div className="col-lg-6">
                                <WatchList
                                    symbol={this.state.symbol}
                                    list={this.state.list}
                                />
                            </div>
                        </div>
                    </div> */}

                </Content>
            </div>
        )
    }
}

export default Watch