import React from 'react'

export default props => {
    return (
        <div className="row">
            <div className="col-lg-3">
                <select
                    className='form-control'
                    value={props.symbol} onChange={props.handleOnChange}>
                    <option value="MSFT">MSFT</option>
                    <option value="AAPL">AAPL</option>
                    <option value="AMZN">AMZN</option>
                </select>
            </div>
        </div>
    )
}