import React, { Component } from 'react'

import ContentHeader from '../common/template/contentHeader'
import Content from '../common/template/content'

import RegistrationForm from './registrationForm'

class Registration extends Component {

    render() {
        return (
            <div>
                <ContentHeader title="Registration" />
                <Content>
                    <RegistrationForm />
                </Content>
            </div>
        )
    }
}

export default Registration