import React, { Component } from 'react'

import InputTextField from '../common/template/inputTextField'
import InputDateField from '../common/template/inputDateField'
import InputCurrencyField from '../common/template/inputCurrencyField'

export default class RegistrationForm extends Component {

    constructor(props) {
        super(props)

        this.state = {
            name: '',
            dob: '',
            netWorth: '',
            address: '',
            country: '',
            state: '',
            city: '',
            selectedRadioOption: 'option2',
            spouseName: '',
            spouseDoB: ''
        }

        this.handleOptionChange = this.handleOptionChange.bind(this)
        this.handleUserInput = this.handleUserInput.bind(this)
    }

    handleOptionChange(e) {
        this.setState({ selectedRadioOption: e.target.value });
    }

    handleUserInput(e) {
        const name = e.target.name
        this.setState(...this.state, { [name]: e.target.value })
    }

    render() {
        return (
            <form>
                <div className="row">
                    <div className="col-md-6">
                        <div className="box box-primary">
                            <div className="box-header">
                                <h3 className="box-title">Informations</h3>
                            </div>
                            <div className="box-body">

                                <InputTextField
                                    label="Name"
                                    placeholder="Name"
                                    name="name"
                                    onChange={this.handleUserInput}
                                    value={this.state.name}
                                />

                                <InputDateField
                                    label="Date of Birth"
                                    name="dob"
                                    onChange={this.handleUserInput}
                                    value={this.state.dob}
                                />

                                <InputCurrencyField
                                    label="Net Worth"
                                    name="netWorth"
                                    onChange={this.handleUserInput}
                                    value={this.state.netWorth}
                                />

                                <InputTextField
                                    label="Address"
                                    placeholder="Address"
                                    name="address"
                                    onChange={this.handleUserInput}
                                    value={this.state.address}
                                />

                                <div className="form-group">
                                    <label>Country</label>
                                    <select id="country-selector" className='form-control'
                                        name="country"
                                        value={this.state.country}
                                        onChange={this.handleUserInput}>
                                        <option value="">Select country</option>
                                        <option value="BRA">Brazil</option>
                                        <option value="USA">United States</option>
                                    </select>
                                </div>

                                <div className="form-group">
                                    <label>State</label>
                                    <select id="state-selector" className='form-control' disabled={this.state.country === ''}
                                        name="state"
                                        value={this.state.state}
                                        onChange={this.handleUserInput}>
                                        <option value="">Select state</option>
                                        <option value="SP">Sao Paulo</option>
                                        <option value="RJ">Rio de Janeiro</option>
                                        <option value="BA">Bahia</option>
                                    </select>
                                </div>

                                <div className="form-group">
                                    <label>City</label>
                                    <select id="city-selector" className='form-control' disabled={this.state.state === ''}
                                        name="city"
                                        value={this.state.city}
                                        onChange={this.handleUserInput}>
                                        <option value="">Select city</option>
                                        <option value="SP">Sao Paulo</option>
                                        <option value="RJ">Santos</option>
                                        <option value="BA">Araraquara</option>
                                    </select>
                                </div>

                            </div>
                        </div>
                    </div>
                    
                    <div className="col-md-6">
                        <div className="box box-info">
                            <div className="box-header">
                                <h3 className="box-title">Join account</h3>
                                <div className="box-body">
                                    <div className="row">

                                        <div className="col-xs-2">
                                            <div className="radio">
                                                <label>
                                                    <input id="join-account-yes" type="radio" value="option1"
                                                        checked={this.state.selectedRadioOption === 'option1'}
                                                        onChange={this.handleOptionChange} />
                                                    Yes
                                                </label>
                                            </div>
                                        </div>
                                        <div className="col-xs-2">
                                            <div className="radio">
                                                <label>
                                                    <input id="join-account-no" type="radio" value="option2"
                                                        checked={this.state.selectedRadioOption === 'option2'}
                                                        onChange={this.handleOptionChange} />
                                                    No
                                                </label>
                                            </div>
                                        </div>

                                    </div>
                                    <div hidden={this.state.selectedRadioOption === 'option2'}>

                                        <InputTextField
                                            label="Spouse Name"
                                            placeholder="Spouse Name"
                                            name="spouseName"
                                            onChange={this.handleUserInput}
                                            value={this.state.spouseName}
                                        />

                                        <InputDateField
                                            label="Spouse - Date of Birth"
                                            name="spouseDoB"
                                            onChange={this.handleUserInput}
                                            value={this.state.spouseDoB}
                                        />
                                    </div>
                                    <div className="box-footer">
                                        <button type="submit" className="btn btn-info pull-right">Submit</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </form>
        )
    }
}
