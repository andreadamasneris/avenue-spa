# Avenue SPA

Avenue SPA

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for testing purposes. 

### Prerequisites

* git
* node
* npm

### Installing

* git clone https://andreadamasneris@bitbucket.org/andreadamasneris/avenue-spa.git
* cd  avenue-spa
* npm install
* npm run dev
* open http://localhost:8080


